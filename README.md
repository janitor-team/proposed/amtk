Amtk - Actions, Menus and Toolbars Kit for GTK applications
===========================================================

Amtk is the acronym for “Actions, Menus and Toolbars Kit”. It is a basic
GtkUIManager replacement based on GAction. It is suitable for both a
traditional UI or a modern UI with a GtkHeaderBar.

More information
----------------

Amtk currently targets **GTK 3**.

[More information about Amtk](docs/more-information.md).
