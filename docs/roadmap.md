Amtk roadmap
============

- See the dev-notes file for possible tasks.

- Port to GTK 4, see if creating a traditional UI with GTK 4 is still (easily)
  possible. If some features are missing wrt GTK 3, fill the gap.
